(require 'package)
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))

;; for tips goto: cjohansen emacs.d github


(setq backup-directory-alist `(("." . "~/.emacs.d/.saves")))
(setq delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)


(add-hook 'after-init-hook (lambda () (load "~/.emacs.d/after-init.el")))



