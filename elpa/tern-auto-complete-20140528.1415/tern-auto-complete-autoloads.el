;;; tern-auto-complete-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (tern-ac-setup) "tern-auto-complete" "tern-auto-complete.el"
;;;;;;  (21398 63993 959472 444000))
;;; Generated autoloads from tern-auto-complete.el

(autoload 'tern-ac-setup "tern-auto-complete" "\
Setup auto-complete for tern-mode.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("tern-auto-complete-pkg.el") (21398 63994
;;;;;;  36317 537000))

;;;***

(provide 'tern-auto-complete-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; tern-auto-complete-autoloads.el ends here
